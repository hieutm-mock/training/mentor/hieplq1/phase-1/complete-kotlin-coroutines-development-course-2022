package hieutm.dev.coroutines.flow.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import hieutm.dev.coroutines.flow.model.NewsRepository

class ListViewModel: ViewModel() {
    val newsArticles = NewsRepository().getNewsArticles().asLiveData()
}