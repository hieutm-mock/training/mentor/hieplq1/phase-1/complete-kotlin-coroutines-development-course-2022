package hieutm.dev.coroutines.coroutine_room.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import hieutm.dev.coroutines.coroutine_room.model.LoginState
import hieutm.dev.coroutines.coroutine_room.model.User
import hieutm.dev.coroutines.coroutine_room.model.UserDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SignupViewModel(application: Application) : AndroidViewModel(application) {

    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    private val db by lazy { UserDatabase(getApplication()).userDao() }

    val signupComplete = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()

    fun signup(username: String, password: String, info: String) {
        coroutineScope.launch {
            val user = db.getUser(username)
            if(user != null) {
                withContext(Dispatchers.Main) {
                    error.value = "User already exists"
                }
            } else {
                val newUser = User(username, password.hashCode(), info)
                val userId = db.insertUser(newUser)
                newUser.id = userId
                LoginState.login(newUser)
                withContext(Dispatchers.Main) {
                    signupComplete.value = true
                }
            }
        }
    }

}