package hieutm.dev.coroutines.coroutine_retrofit.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hieutm.dev.coroutines.coroutine_retrofit.model.Country
import hieutm.dev.coroutines.databinding.ItemCountryBinding

class CountryListAdapter(var countries: ArrayList<Country>): RecyclerView.Adapter<CountryListAdapter.CountryViewHolder>() {
    private lateinit var _binding: ItemCountryBinding
    private val binding get() = _binding
    fun updateCountries(newCountries: List<Country>) {
        countries.clear()
        countries.addAll(newCountries)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CountryViewHolder {
        _binding = ItemCountryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CountryViewHolder(binding)
    }

    override fun getItemCount() = countries.size

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(countries[position])
    }

    class CountryViewHolder(private val binding: ItemCountryBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(country: Country) {
            binding.name.text = country.countryName
            binding.capital.text = country.capital
            binding.imageView.loadImage(country.flag)
        }
    }
}