package hieutm.dev.coroutines.coroutine_room.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import hieutm.dev.coroutines.R
import hieutm.dev.coroutines.coroutine_room.viewmodel.SignupViewModel
import hieutm.dev.coroutines.databinding.FragmentSignupBinding

class SignupFragment : Fragment() {

    private lateinit var viewModel: SignupViewModel
    private lateinit var _binding: FragmentSignupBinding
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSignupBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.signupBtn.setOnClickListener { onSignup() }
        binding.gotoLoginBtn.setOnClickListener { onGotoLogin(it) }

        viewModel = ViewModelProviders.of(this)[SignupViewModel::class.java]
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.signupComplete.observe(viewLifecycleOwner) {
            Toast.makeText(activity, "Signup complete", Toast.LENGTH_SHORT).show()
            val action = SignupFragmentDirections.actionGoToMain()
            Navigation.findNavController(binding.signupUsername).navigate(action)
        }

        viewModel.error.observe(viewLifecycleOwner) { error ->
            Toast.makeText(activity, "Error: $error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onSignup(){
        val username = binding.signupUsername.text.toString()
        val password = binding.signupPassword.text.toString()
        val info = binding.otherInfo.text.toString()
        if(username.isNullOrEmpty() || password.isNullOrEmpty() || info.isNullOrEmpty()) {
            Toast.makeText(activity, "Please fill all fields", Toast.LENGTH_SHORT).show()
        } else {
            viewModel.signup(username, password, info)
        }
    }

    private fun onGotoLogin(v: View) {
        val action = SignupFragmentDirections.actionGoToLogin()
        Navigation.findNavController(v).navigate(action)
    }
}
