package hieutm.dev.coroutines.coroutine_room.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import hieutm.dev.coroutines.coroutine_room.model.LoginState
import hieutm.dev.coroutines.coroutine_room.viewmodel.MainViewModel
import hieutm.dev.coroutines.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var _binding: FragmentMainBinding
    private val binding get() =  _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.usernameTV.text = LoginState.user?.username
        binding.signoutBtn.setOnClickListener { onSignOut() }
        binding.deleteUserBtn.setOnClickListener { onDelete() }

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        observerViewModel()
    }

    private fun observerViewModel() {
        viewModel.signOut.observe(viewLifecycleOwner) {
            Toast.makeText(activity, "Signed out", Toast.LENGTH_SHORT).show()
            goToSignupScreen()
        }
        viewModel.userDeleted.observe(viewLifecycleOwner) {
            Toast.makeText(activity, "User deleted", Toast.LENGTH_SHORT).show()
            goToSignupScreen()
        }
    }

    private fun goToSignupScreen() {
        val action = MainFragmentDirections.actionGoToSignup()
        Navigation.findNavController(binding.usernameTV).navigate(action)
    }

    private fun onSignOut() {
        viewModel.onSignOut()
    }

    private fun onDelete() {
        activity?.let {
            AlertDialog.Builder(it)
                .setTitle("Delete user")
                .setMessage("Are you sure you want to delete this user?")
                .setPositiveButton("Yes") { _, _ -> viewModel.onDeleteUser()}
                .setNegativeButton("Cancel", null)
                .create()
                .show()
        }
    }

}
