package hieutm.dev.coroutines.flow.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hieutm.dev.coroutines.databinding.ItemNewsArticleBinding
import hieutm.dev.coroutines.flow.model.NewsArticle

class NewsListAdapter: RecyclerView.Adapter<NewsListAdapter.NewsItemViewHolder>() {

    private val newsItems = arrayListOf<NewsArticle>()
    private lateinit var _binding: ItemNewsArticleBinding
    private val binding get() = _binding

    fun onAddNewsItem(item: NewsArticle) {
        newsItems.add(0, item)
        notifyItemInserted(0)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NewsItemViewHolder {
        _binding = ItemNewsArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NewsItemViewHolder(binding)
    }

    override fun getItemCount() = newsItems.size

    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) {
        holder.bind(newsItems[position])
    }

    class NewsItemViewHolder(private val binding: ItemNewsArticleBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(newsItem: NewsArticle) {
            binding.newsImage.loadImage(newsItem.urlToImage)
            binding.newsAuthor.text = newsItem.author
            binding.newsTitle.text = newsItem.title
            binding.newsPublishedAt.text = newsItem.publishedAt
        }
    }
}