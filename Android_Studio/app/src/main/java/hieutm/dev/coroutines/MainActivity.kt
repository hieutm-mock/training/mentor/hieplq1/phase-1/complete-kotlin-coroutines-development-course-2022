package hieutm.dev.coroutines

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hieutm.dev.coroutines.coroutine_retrofit.view.CoroutineRetrofit
import hieutm.dev.coroutines.coroutine_room.view.CoroutineRoom
import hieutm.dev.coroutines.databinding.ActivityMainBinding
import hieutm.dev.coroutines.flow.view.CoroutineFlow
import hieutm.dev.coroutines.image_processing.ImageProcessingCoroutines

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.imageProcessingBtn.setOnClickListener {
            startActivity(Intent(this, ImageProcessingCoroutines::class.java))
        }

        binding.coroutineRetrofitBnt.setOnClickListener {
            startActivity(Intent(this, CoroutineRetrofit::class.java))
        }

        binding.coroutineRoomBnt.setOnClickListener {
            startActivity(Intent(this, CoroutineRoom::class.java))
        }

        binding.flowBtn.setOnClickListener {
            startActivity(Intent(this, CoroutineFlow::class.java))
        }
    }
}