package hieutm.dev.coroutines.coroutine_retrofit.view

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import hieutm.dev.coroutines.R

fun ImageView.loadImage(uri: String?) {
    val options = RequestOptions().error(R.mipmap.ic_launcher_round)
    Glide.with(this.context).setDefaultRequestOptions(options).load(uri).into(this)
}