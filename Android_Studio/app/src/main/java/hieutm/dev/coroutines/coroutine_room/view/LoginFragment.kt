package hieutm.dev.coroutines.coroutine_room.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import hieutm.dev.coroutines.coroutine_room.viewmodel.LoginViewModel
import hieutm.dev.coroutines.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var _binding: FragmentLoginBinding
    private val binding get() = _binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginBtn.setOnClickListener { onLogin() }
        binding.gotoSignupBtn.setOnClickListener { onGotoSignup(it) }

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        observerViewModel()
    }

    private fun observerViewModel() {
        viewModel.loginComplete.observe(viewLifecycleOwner) {
            Toast.makeText(activity, "Login complete", Toast.LENGTH_SHORT).show()
            val action = LoginFragmentDirections.actionGoToMain()
            Navigation.findNavController(binding.loginUsername).navigate(action)
        }

        viewModel.error.observe(viewLifecycleOwner) { error ->
            Toast.makeText(activity, "Error: $error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onLogin() {
        val username = binding.loginUsername.text.toString()
        val password = binding.loginPassword.text.toString()
        if(username.isNullOrEmpty() || password.isNullOrEmpty()) {
            Toast.makeText(activity, "Please fill all fields", Toast.LENGTH_SHORT).show()
        } else {
            viewModel.login(username, password)
        }
    }

    private fun onGotoSignup(v: View){
        val action = LoginFragmentDirections.actionGoToSignup()
        Navigation.findNavController(v).navigate(action)
    }
}
