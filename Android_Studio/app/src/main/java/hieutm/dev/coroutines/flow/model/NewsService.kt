package hieutm.dev.coroutines.flow.model

import hieutm.dev.coroutines.flow.model.NewsArticle
import retrofit2.http.GET

interface NewsService {
    @GET("news.json")
    suspend fun getNews(): List<NewsArticle>
}